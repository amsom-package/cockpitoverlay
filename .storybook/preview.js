/** @type { import('@storybook/vue3').Preview } */
import '@amsom-habitat/bootstrap-5/dist/index.css';

const preview = {
  parameters: {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i
      }
    }
  }
}

export default preview
