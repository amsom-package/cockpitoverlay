module.exports = {
  extends: ['plugin:vue/vue3-recommended', '@vue/eslint-config-prettier/skip-formatting', 'plugin:storybook/recommended'],
  rules: {
    // override/add rules settings here, such as:
    // 'vue/no-unused-vars': 'error'
    'vue/eqeqeq': 1, //erreur du package
    'vue/array-bracket-newline': 1,
    'vue/array-bracket-spacing': 1,
    'vue/arrow-spacing': 1,
    'vue/comma-dangle': 1,
    'vue/comma-spacing': 1,
    'vue/object-curly-newline': 1,
    'vue/object-property-newline': 1,



    'vue/component-options-name-casing': 1,
    'vue/html-comment-content-newline': 1,
    'vue/html-comment-content-spacing': 1,
    'vue/html-comment-indent': 1,
    'vue/match-component-import-name': 1,
    // 'vue/match-component-file-name': 1,
    'vue/new-line-between-multi-line-property': 1,
    'vue/no-multiple-objects-in-class': 2,
    'vue/no-potential-component-option-typo': 1,
    // 'vue/no-ref-object-destructure': 1, //marche pas
    // 'vue/no-required-prop-with-default': 1,  //marche pas
    // 'vue/no-undef-components': 2, //non vueX friendly
    // 'vue/no-undef-properties': 2, //non vueX friendly
    'vue/no-unused-properties': 2, //error
    'vue/no-unused-refs': 2, //error
    'vue/no-useless-v-bind': 2, //error
    'vue/padding-line-between-blocks': 1,
    // 'vue/padding-line-between-tags': 1, //marche pas
    // 'vue/padding-lines-in-component-definition': 1, //marche pas
    'vue/prefer-separate-static-class': 1,
    'vue/prefer-true-attribute-shorthand': 1,
    'vue/require-direct-export': 1,
    'vue/require-name-property': 2, //error
    // 'vue/require-emit-validator': 1,
    // 'vue/script-indent': 1,
    'vue/component-name-in-template-casing': [2, "kebab-case"]
  }
}

