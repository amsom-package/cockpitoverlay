# Changelog
<!-- introduction au changelog -->
## V0.10.0 - 19/06/2024

### Changed

- Mise à jour de l'UI

## V0.9.0 - 19/06/2024

### Changed

- Mise à jour de l'UI

## V0.7.0 - 19/06/2024

### Changed

- Mise à jour de l'UI

## V0.6.0 - 19/06/2024

### Changed

- Mise à jour du panneau et du user manager

## V0.3.0 - 19/06/2024

### Added

- Ajout de sollicitations

### Changed

- Liens vers les pages paramétrables

## V0.1.0 - 11/06/2024

### Added

- Première version stable de l'overlay

## V0.0.0 - 05/03/2023

### Added

- blabla
- blabla

### Fixed

- blabla
- blabla

### Changed

- blabla
- blabla

### Removed

- blabla
- blabla
