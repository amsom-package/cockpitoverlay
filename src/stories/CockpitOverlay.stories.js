import CockpitOverlay from '../CockpitOverlay.vue'

const templateTopBar = `
<div style="height: 50px; background-color: aqua" class="position-fixed d-flex align-items-center bg-gray w-100">
<cockpit-overlay v-bind="args"/>
</div>
lorem ipsum dolor sit amet consectetur adipisicing elit
`


// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
export default {
  title: 'CockpitOverlay',
  component: CockpitOverlay,
  tags: ['autodocs'],
  argTypes: {
    //backgroundColor: {
    //  control: 'color',
    //},
    //onClick: {},
    //size: {
    //  control: {
    //    type: 'select',
    //  },
    //  options: ['small', 'medium', 'large'],
    //},
  }
}

export const Default = {
  component: CockpitOverlay,
  tags: ['autodocs'],
  argTypes: {
  },
}

export const TemplateTopBar = {
  render: (args) => ({
    template: templateTopBar,
    components: { CockpitOverlay }
  }),
}
